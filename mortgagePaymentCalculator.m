function mortgagePaymentCalculator
%Use for analyzing mortgage payment
%plot debt remaining & calculate monthly mortgage payment. Assume interest
%rate fixed
%
%Problem definition
P=2e5; %principal loan taken at t=0
I=4.5e-2/12; %monthly interest rate used to calculate new loan, ie, current monthly loan=(prev monthly loan)(1+I), monthly interest rate = annual rate/12
N=360; %total # of mortage payment to payback principal with monthly interest
l=N+2; %total # of sim pt. from n=0 onward. Due to initial rest condition, x[n<=0] = y[n<=0] = 0. First nonzero value occur at n=1
M=1013.37; %monthly mortgage payment
M1=P/sum((1/(1+I)+zeros(1,N)).^[1:N]) %calculated monthly payment to be debt free by n=N using Z-transform to solve difference eq
M2=P*(1/(1+I)-1)/(1/(1+I)*(1/(1+I)^N-1)) %calculated monthly payment to be debt free by n=N, simplified summation by hand
x=P*impulse0(l,1)-M*pulse(l,2,N+1); %input stimulus to system with P debt at n=1 & M payment for 2<=n<=N+1
y=calcYdiff(size(x,1),x,I); %y calculated iteratively using linear constant coe diff eq
plot(y);
y(360:362)

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function d=impulse0(length,onePt)
%Create impulse with # of sample = length, & 1@sample index = onePt
%(zero-based), 0 everywhere else
d=zeros(length,1);
d(onePt+1)=1;
function d=pulse(length,stPt,edPt)
%create retangular pulse with total # of sample = length, starting index
%for pulse at stPt (0-indexed), ending index at edPt. Within pulse width
%defined by stPt & edPt (inclusive), pulse=1. Outside width, pulse=0
d=zeros(length,1);
for i=stPt:edPt
    d(i+1)=1;
end
function y=calcYdiff(length, x, I)
%calc y value based on linear, const coe difference eq:
%1/(1+I)*y[n]-y[n-1]=x[n] or y[n]=(1+I)(y[n-1]+x[n])
%assume initial rest => y[n<=0]=x[n<=0]=0
%
%length is # of y value to calc
%x is input stimulus to system
%I is interest rate
y=zeros(length,1);
for i=2:length
    y(i)=(1+I)*(y(i-1)+x(i));
end