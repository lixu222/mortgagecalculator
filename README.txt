calculate monthly mortgage payment based on fixed term & interest rate for loan

contain detailed derivation and solution of linear, constant coefficient difference equation used to model mortgage.

also contain matlab file to calculate mortgage based on analytical solution and recursive mortgage debt calculation directly from difference equation
